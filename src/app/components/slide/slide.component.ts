import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.css']
})
export class SlideComponent implements OnInit {
  @Input()
  public name: string;

  @Input()
  public size: string;

  @Input()
  public url: string;

  constructor(){
    this.name = "Default Title";
    this.size = "big";
    this.url = "";
  }

  ngOnInit(): void {
  }

}
